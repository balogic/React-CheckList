import React from 'react'
import Table from './Table'
const items = [
  {
    category: "Sporting Goods",
    price: "$49.99",
    stocked: true,
    name: "Football"
  }, {
    category: "Sporting Goods",
    price: "$9.99",
    stocked: true,
    name: "Baseball"
  }, {
    category: "Sporting Goods",
    price: "$29.99",
    stocked: false,
    name: "Basketball"
  }, {
    category: "Electronics",
    price: "$99.99",
    stocked: true,
    name: "iPod Touch"
  }, {
    category: "Electronics",
    price: "$399.99",
    stocked: false,
    name: "iPhone 5"
  }, {
    category: "Electronics",
    price: "$199.99",
    stocked: true,
    name: "Nexus 7"
  }
]
class ItemList extends React.Component {
  render() {
    const finalItems = []
    items.map((item) => {
      if ((item.name.indexOf(this.props.searchText.value)) === 0) {
        if (this.props.searchText.check) {
          if (this.props.searchText.check === item.stocked) {
            finalItems.push(item)
          }
        } else {
          finalItems.push(item)
        }
      }
      return finalItems
    })
    return (
      <div>
        <Table items={finalItems}/>
      </div>
    )
  }
}
export default ItemList;
