import React from 'react'
class Table extends React.Component {
  render() {
    return (
      <div>
        <table border="1">
          <thead>
          <tr>
            <th>
              Price
            </th>
            <th>
              Item
            </th>
          </tr>
        </thead>
          <tbody>
          {this.props.items.map((item) => (
              <tr key={item.name}>
                <td>
                  {item.price}
                </td>
                <td>
                  {item.name}
                </td>
              </tr>
          ))}
        </tbody>
        </table>
      </div>
    )
  }
}
export default Table;
