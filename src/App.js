import React from 'react'
import ItemList from './ItemList'
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      value: "",
      check: false
    }
    this.updateChange = this.updateChange.bind(this);
    this.updateStock = this.updateStock.bind(this);
  }
  updateChange(e) {
    this.setState({value: e.target.value})
  }
  updateStock() {
    console.log(document.getElementById("checkstock").checked);
    this.setState({check: (document.getElementById("checkstock").checked)})
  }
  render() {
    return (
      <div>
        <h2>Shop List</h2>
        <input type="text" onChange={this.updateChange} value={this.state.value}/>
        <h2>{this.state.check}</h2>
        <br></br>
        <div>
          <input id="checkstock" type="checkbox" onChange={this.updateStock}/>
          <label>Show only Stocked</label>
        </div>
        <br></br>
        <ItemList searchText={this.state}/>
      </div>
    )
  }
}
export default App;
